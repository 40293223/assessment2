﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HolidayManager.Windows;

namespace HolidayManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            AddCustomer ad = new AddCustomer();
            ad.ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            EditCustomer ed = new EditCustomer();
            ed.ShowDialog();
        }

        private void AddBooking_OnClick(object sender, RoutedEventArgs e)
        {
            AddBooking ad = new AddBooking();
            ad.ShowDialog();
        }

        private void EditBooking_OnClick(object sender, RoutedEventArgs e)
        {
            EditBooking ed= new EditBooking();
            ed.ShowDialog();
        }

        private void AddEditGuests_Click(object sender, RoutedEventArgs e)
        {
            AddEditGuest ag = new AddEditGuest();
            ag.ShowDialog();
        }

        private void AddEditExtras_Click(object sender, RoutedEventArgs e)
        {
            AddEditExtra ae = new AddEditExtra();
            ae.ShowDialog();
        }

        private void AddEditCarHire_Click(object sender, RoutedEventArgs e)
        {
            Windows.AddEditCarHire ac = new AddEditCarHire();
            ac.ShowDialog();
        }

        private void GenerateInvoice_Click(object sender, RoutedEventArgs e)
        {
            InvoiceDetails id = new InvoiceDetails();
            id.ShowDialog();
        }
    }
}
