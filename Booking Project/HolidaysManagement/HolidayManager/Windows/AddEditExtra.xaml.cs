﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HolidaysManagement.BL;
using HolidaysManagement.Common.Schema;

namespace HolidayManager.Windows
{
    /// <summary>
    /// Interaction logic for AddEditExtra.xaml
    /// </summary>
    public partial class AddEditExtra : Window
    {
        private ObservableCollection<Customer> _customers = null;
        private ObservableCollection<Booking> _bookings = null;
        private ObservableCollection<Extras> _extras = null;
        public AddEditExtra()
        {
            InitializeComponent();
            DataGridBooking.Visibility = Visibility.Collapsed;
            DataGridCustomer.Visibility = Visibility.Collapsed;
            DataGridExtras.Visibility = Visibility.Collapsed;
            GroupBoxExtras.Visibility = Visibility.Collapsed;
            TextBoxBf.IsEnabled = false;
            TextBoxEve.IsEnabled = false;
            CheckBoxBf.IsChecked = false;
        }

        private void CheckBoxSame_OnChecked(object sender, RoutedEventArgs e)
        {
            DatePickerExtrasDate.Visibility = Visibility.Collapsed;
        }

        private void CheckBoxBf_OnChecked(object sender, RoutedEventArgs e)
        {
            TextBoxBf.IsEnabled = CheckBoxBf.IsChecked.Value;
        }

        private void CheckBoxEve_OnChecked(object sender, RoutedEventArgs e)
        {
            TextBoxEve.IsEnabled = CheckBoxEve.IsChecked.Value;
        }
        //Search customer button click, will search for the customer similiar to the name in the textbox
        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DataGridCustomer.Visibility = Visibility.Visible;
            DataGridBooking.Visibility = Visibility.Collapsed;
            DataGridExtras.Visibility = Visibility.Collapsed;
            GroupBoxExtras.Visibility = Visibility.Collapsed;
            DatePickerExtrasDate.Visibility = Visibility.Collapsed;
            TextBoxBf.IsEnabled = false;
            TextBoxEve.IsEnabled = false;
            var lst = BusinessManager.BusinessMgrInstance.SearchCustomer(TextBoxsearch.Text, ConfigurationManager.AppSettings["ConnectionString"]);
            _customers = new ObservableCollection<Customer>(lst);
            DataGridCustomer.Visibility = Visibility.Visible;
            DataGridCustomer.ItemsSource = _customers;
        }
        //this will select the specific customer and will fetch booking details for it in the datagrid
        private void ButtonBase_OnSelectClick(object sender, RoutedEventArgs e)
        {
            Customer obj = ((FrameworkElement)sender).DataContext as Customer;
            _bookings = new ObservableCollection<Booking>(obj.BookingDetails);
            if (_bookings.Count > 0)
            {
                DataGridCustomer.Visibility = Visibility.Collapsed;
                DataGridBooking.Visibility = Visibility.Visible;
                DataGridBooking.ItemsSource = _bookings;
                TextBlockCustomerId.Text = Convert.ToString(obj.CustomerReferenceId);
            }
            else
            {
                MessageBox.Show("No bookings for this customer. please add booking first.");
            }
        }
        //this method will select specific booking and will fetch guest details for the booking id, and populate in datagrid
        private void ButtonBase_OnSelectBookingClick(object sender, RoutedEventArgs e)
        {
            DataGridCustomer.Visibility = Visibility.Collapsed;
            DataGridBooking.Visibility = Visibility.Collapsed;
            DataGridExtras.Visibility = Visibility.Visible;
            GroupBoxExtras.Visibility = Visibility.Visible;
            Booking obj = ((FrameworkElement)sender).DataContext as Booking;
            TextBlockBookingId.Text = Convert.ToString(obj.BookingReferenceId);
            var bookingDetails = BusinessManager.BusinessMgrInstance.FetchExtrasGuestDetails(Convert.ToInt32(obj.BookingReferenceId), ConfigurationManager.AppSettings["ConnectionString"]);
            _extras = new ObservableCollection<Extras>(bookingDetails.ExtrasList);
            DataGridExtras.ItemsSource = _extras;
            DatePickerExtrasDate.DisplayDateStart = obj.ArrivalDate;
            DatePickerExtrasDate.DisplayDateEnd = obj.DepartureDate;
        }
        //edits the extras row on which edit is selected
        private void ButtonBase_OnEditClick(object sender, RoutedEventArgs e)
        {
            Extras obj = ((FrameworkElement)sender).DataContext as Extras;
            CheckBoxSame.IsChecked = obj.HasOptedSameForAllDays;
            CheckBoxBf.IsChecked = obj.HasOptedForBreakfast;
            CheckBoxEve.IsChecked = obj.HasOptedForEveningMeal;
            TextBoxEve.Text = obj.EveningMealRequirements;
            TextBoxBf.Text = obj.BreakfastRequirements;
            TextblockExtrasId.Text = Convert.ToString(obj.BookingExtrasDetailsId);
        }
        //delete the extras details from the row
        private void ButtonBase_OnDeleteClick(object sender, RoutedEventArgs e)
        {
            Extras obj = ((FrameworkElement)sender).DataContext as Extras;
            var ret = obj != null && BusinessManager.BusinessMgrInstance.DeleteExtras(obj.BookingExtrasDetailsId, ConfigurationManager.AppSettings["ConnectionString"]);
            if (ret)
            {
                MessageBox.Show("Deleted successfully");
                _extras.Remove(obj);
            }
            else
            {
                MessageBox.Show("Error while deleting. please try again.");
            }

        }
        //saves the extras details in the database
        private void ButtonBase_OnSaveClick(object sender, RoutedEventArgs e)
        {
            if (!CheckBoxSame.IsChecked.Value && DatePickerExtrasDate.SelectedDate == null)
            {
                MessageBox.Show("Please select date");
                return;
            }
            if (CheckBoxSame.IsChecked.Value && (!CheckBoxBf.IsChecked.Value && !CheckBoxEve.IsChecked.Value))
            {
                return;
            }
            if (!CheckBoxSame.IsChecked.Value && (!CheckBoxBf.IsChecked.Value && !CheckBoxEve.IsChecked.Value))
            {
                return;
            }
            var ret = false;
            if (string.IsNullOrEmpty(TextblockExtrasId.Text))
            {
                ret = BusinessManager.BusinessMgrInstance.InsertExtras(Convert.ToInt32(TextBlockBookingId.Text), CheckBoxSame.IsChecked.Value, DatePickerExtrasDate.SelectedDate, CheckBoxBf.IsChecked.Value, TextBoxBf.Text, CheckBoxEve.IsChecked.Value, TextBoxEve.Text, ConfigurationManager.AppSettings["ConnectionString"]);
            }
            else
            {
                ret = BusinessManager.BusinessMgrInstance.UpdateExtras(Convert.ToInt32(TextblockExtrasId.Text), CheckBoxSame.IsChecked.Value, DatePickerExtrasDate.SelectedDate, CheckBoxBf.IsChecked.Value, TextBoxBf.Text, CheckBoxEve.IsChecked.Value, TextBoxEve.Text, ConfigurationManager.AppSettings["ConnectionString"]);
            }
            if (ret)
            {
                MessageBox.Show("Saved successfully");
            }
            else
            {
                MessageBox.Show("Error while saving. please try again.");
            }
            var bookingDetails = BusinessManager.BusinessMgrInstance.FetchExtrasGuestDetails(Convert.ToInt32(TextBlockBookingId.Text), ConfigurationManager.AppSettings["ConnectionString"]);
            _extras = new ObservableCollection<Extras>(bookingDetails.ExtrasList);
            DataGridExtras.ItemsSource = _extras;
        }

        private void CheckBoxSame_OnUnchecked(object sender, RoutedEventArgs e)
        {
            DatePickerExtrasDate.Visibility = Visibility.Visible;
        }
    }
}
