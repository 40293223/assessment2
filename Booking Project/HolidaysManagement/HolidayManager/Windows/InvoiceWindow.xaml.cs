﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HolidaysManagement.Common.Schema;

namespace HolidayManager.Windows
{
    /// <summary>
    /// Interaction logic for InvoiceWindow.xaml
    /// </summary>
    public partial class InvoiceWindow : Window
    {
        public InvoiceWindow()
        {
            InitializeComponent();
        }
        //sets the invoice details in the winodw
        public InvoiceWindow(Invoice invoice) : this()
        {
            TextBlockBookingId.Text = Convert.ToString(invoice.BookingReferenceId);
            TextBlockBasicCost.Text = Convert.ToString(invoice.TotalBasicCost);
            TextBlockBreakfast.Text = Convert.ToString(invoice.BreakfastCost);
            TextBlockEveningMeal.Text = Convert.ToString(invoice.EveningMealCost);
            TextBlockCarHire.Text = Convert.ToString(invoice.CarHireCost);
            TextBlockNumberDays.Text = Convert.ToString(invoice.NoOfDaysInBooking);
            TextBlockTotalAmount.Text = Convert.ToString(invoice.TotalCost);
        }
    }
}
