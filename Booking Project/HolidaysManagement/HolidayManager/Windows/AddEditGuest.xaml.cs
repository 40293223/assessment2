﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HolidaysManagement.BL;
using HolidaysManagement.Common.Schema;

namespace HolidayManager.Windows
{
    /// <summary>
    /// Interaction logic for AddEditGuest.xaml
    /// </summary>
    public partial class AddEditGuest : Window
    {
        private ObservableCollection<Customer> _customers = null;
        private ObservableCollection<Booking> _bookings = null;
        private ObservableCollection<Guest> _guests = null;
        public AddEditGuest()
        {
            InitializeComponent();
            DataGridBooking.Visibility = Visibility.Collapsed;
            DataGridCustomer.Visibility = Visibility.Collapsed;
            DataGridGuest.Visibility = Visibility.Collapsed;
            GroupBoxGuest.Visibility = Visibility.Collapsed;
        }
        //Search customer button click, will search for the customer similiar to the name in the textbox
        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DataGridCustomer.Visibility = Visibility.Visible;
            DataGridBooking.Visibility = Visibility.Collapsed;
            DataGridGuest.Visibility = Visibility.Collapsed;
            GroupBoxGuest.Visibility = Visibility.Collapsed;
            var lst = BusinessManager.BusinessMgrInstance.SearchCustomer(TextBoxsearch.Text, ConfigurationManager.AppSettings["ConnectionString"]);
            _customers = new ObservableCollection<Customer>(lst);
            DataGridCustomer.Visibility = Visibility.Visible;
            DataGridCustomer.ItemsSource = _customers;
        }

        //this will select the specific customer and will fetch booking details for it in the datagrid
        private void ButtonBase_OnSelectClick(object sender, RoutedEventArgs e)
        {
            Customer obj = ((FrameworkElement)sender).DataContext as Customer;
            _bookings = new ObservableCollection<Booking>(obj.BookingDetails);
            if (_bookings.Count > 0)
            {
                DataGridCustomer.Visibility = Visibility.Collapsed;
                DataGridBooking.Visibility = Visibility.Visible;
                DataGridBooking.ItemsSource = _bookings;
                TextBlockCustomerId.Text = Convert.ToString(obj.CustomerReferenceId);
            }
            else
            {
                MessageBox.Show("No bookings for this customer. please add booking first.");
            }
            
        }
        //this method will select specific booking and will fetch guest details for the booking id, and populate in datagrid
        private void ButtonBase_OnSelectBookingClick(object sender, RoutedEventArgs e)
        {
            DataGridCustomer.Visibility = Visibility.Collapsed;
            DataGridBooking.Visibility = Visibility.Collapsed;
            DataGridGuest.Visibility = Visibility.Visible;
            GroupBoxGuest.Visibility = Visibility.Visible;
            Booking obj = ((FrameworkElement)sender).DataContext as Booking;
            TextBlockBookingId.Text = Convert.ToString(obj.BookingReferenceId);
            var bookingDetails = BusinessManager.BusinessMgrInstance.FetchExtrasGuestDetails(Convert.ToInt32(obj.BookingReferenceId), ConfigurationManager.AppSettings["ConnectionString"]);
            _guests = new ObservableCollection<Guest>(bookingDetails.GuestList);
            DataGridGuest.ItemsSource = _guests;
        }

        private void AgeTb_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(AgeTb.Text) < 0 || Convert.ToInt32(AgeTb.Text) > 101)
                {
                    AgeTb.Text = 0.ToString();
                }
            }
            catch (Exception ex)
            {
                AgeTb.Text = 0.ToString();
            }
        }
        //when user clicks on save button, this will save the guests details in database
        private void ButtonBase_OnSaveClick(object sender, RoutedEventArgs e)
        {
            if (_guests.Count == 4)
            {
                MessageBox.Show("You cannot add more than 4 guests");
                return;
            }
            if (string.IsNullOrEmpty(TextBlockGuesName.Text) || string.IsNullOrEmpty(TextBlockPassPortNumber.Text) || string.IsNullOrEmpty(AgeTb.Text))
            {
                MessageBox.Show("Please enter fields correctly");
                return;
            }
            var ret = false;
            if (string.IsNullOrEmpty(TextblockGuestId.Text))
            {
                ret = BusinessManager.BusinessMgrInstance.InsertGuest(Convert.ToInt32(TextBlockBookingId.Text), TextBlockGuesName.Text, TextBlockPassPortNumber.Text, Convert.ToInt32(AgeTb.Text), ConfigurationManager.AppSettings["ConnectionString"]);
            }
            else
            {
                ret = BusinessManager.BusinessMgrInstance.UpdateGuest(Convert.ToInt32(TextblockGuestId.Text), TextBlockGuesName.Text, TextBlockPassPortNumber.Text, Convert.ToInt32(AgeTb.Text), ConfigurationManager.AppSettings["ConnectionString"]);
            }
            if (ret)
            {
                MessageBox.Show("Saved successfully");
            }
            else
            {
                MessageBox.Show("Error while saving. please try again.");
            }
            var bookingDetails = BusinessManager.BusinessMgrInstance.FetchExtrasGuestDetails(Convert.ToInt32(TextBlockBookingId.Text), ConfigurationManager.AppSettings["ConnectionString"]);
            _guests = new ObservableCollection<Guest>(bookingDetails.GuestList);
            DataGridGuest.ItemsSource = _guests;
            TextblockGuestId.Text = "";
            TextBlockGuesName.Text = "";
            TextBlockPassPortNumber.Text = "";
            AgeTb.Text = "";
        }

        //When user clicks edit button in the guest datagrid it will select the guest and update the textboxes
        private void ButtonBase_OnEditClick(object sender, RoutedEventArgs e)
        {
            Guest obj = ((FrameworkElement)sender).DataContext as Guest;
            AgeTb.Text = Convert.ToString(obj.Age);
            TextblockGuestId.Text = Convert.ToString(obj.BookingGuestId);
            TextBlockPassPortNumber.Text = Convert.ToString(obj.PassportNumber);
            TextBlockGuesName.Text = obj.Name;
        }
        //When user clicks delete button in the guest datagrid it will delete the guest from database
        private void ButtonBase_OnDeleteClick(object sender, RoutedEventArgs e)
        {
            Guest obj = ((FrameworkElement)sender).DataContext as Guest;
            var ret = obj != null && BusinessManager.BusinessMgrInstance.DeleteGuest(obj.BookingGuestId, ConfigurationManager.AppSettings["ConnectionString"]);
            if (ret)
            {
                MessageBox.Show("Deleted successfully");
                _guests.Remove(obj);
            }
            else
            {
                MessageBox.Show("Error while deleting. please try again.");
            }

        }
    }
}
