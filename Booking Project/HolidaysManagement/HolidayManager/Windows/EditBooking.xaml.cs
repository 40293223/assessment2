﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HolidaysManagement.BL;
using HolidaysManagement.Common.Schema;

namespace HolidayManager.Windows
{
    /// <summary>
    /// Interaction logic for EditBooking.xaml
    /// </summary>
    public partial class EditBooking : Window
    {
        private ObservableCollection<Customer> _customers = null;
        private ObservableCollection<Booking> _bookings = null;
        public EditBooking()
        {
            InitializeComponent();
            DataGridBooking.Visibility = Visibility.Collapsed;
            DataGridCustomer.Visibility = Visibility.Collapsed;
            GroupBoxBooking.Visibility = Visibility.Collapsed;
        }
        //edits the booking details of the row in which eidt button is clicked
        private void ButtonBase_OnEditClick(object sender, RoutedEventArgs e)
        {
            DataGridCustomer.Visibility = Visibility.Collapsed;
            DataGridBooking.Visibility = Visibility.Collapsed;
            GroupBoxBooking.Visibility = Visibility.Visible;
            Booking obj = ((FrameworkElement)sender).DataContext as Booking;
            DatePickerDepDate.SelectedDate = obj.DepartureDate;
            DatePickerArrivalDate.SelectedDate = obj.ArrivalDate;
            TextBlockCustomerId.Text = Convert.ToString(obj.CustomerReferenceId);
            TextBlockBookingId.Text = Convert.ToString(obj.BookingReferenceId);
        }
        //deletes the booking details of the row in which eidt button is clicked
        private void ButtonBase_OnDeleteClick(object sender, RoutedEventArgs e)
        {
            Booking obj = ((FrameworkElement)sender).DataContext as Booking;
            var ret = BusinessManager.BusinessMgrInstance.DeleteBooking(Convert.ToInt32(obj.BookingReferenceId), ConfigurationManager.AppSettings["ConnectionString"]);
            if (ret)
            {
                MessageBox.Show("Deleted successfully");
                _bookings.Remove(obj);
            }
            else
            {
                MessageBox.Show("Error while deleting. please try again.");
            }

        }
        //Search customer button click, will search for the customer similiar to the name in the textbox
        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DataGridCustomer.Visibility = Visibility.Visible;
            DataGridBooking.Visibility = Visibility.Collapsed;
            GroupBoxBooking.Visibility = Visibility.Collapsed;
            var lst = BusinessManager.BusinessMgrInstance.SearchCustomer(TextBoxsearch.Text, ConfigurationManager.AppSettings["ConnectionString"]);
            _customers = new ObservableCollection<Customer>(lst);
            DataGridCustomer.Visibility = Visibility.Visible;
            DataGridCustomer.ItemsSource = _customers;
        }
        //this will select the specific customer and will fetch booking details for it in the datagrid
        private void ButtonBase_OnSelectClick(object sender, RoutedEventArgs e)
        {
            Customer obj = ((FrameworkElement)sender).DataContext as Customer;
            _bookings = new ObservableCollection<Booking>(obj.BookingDetails);
            if (_bookings.Count > 0)
            {
                DataGridCustomer.Visibility = Visibility.Collapsed;
                DataGridBooking.Visibility = Visibility.Visible;
                DataGridBooking.ItemsSource = _bookings;
                TextBlockCustomerId.Text = Convert.ToString(obj.CustomerReferenceId);
            }
            else
            {
                MessageBox.Show("No bookings for this customer. please add booking first.");
            }
        }
        //Saves the booking in the database
        private void ButtonBase_OnSaveClick(object sender, RoutedEventArgs e)
        {
            var ret = false;
            if (DatePickerDepDate.SelectedDate != null && (DatePickerArrivalDate.SelectedDate != null))
            {
                ret = BusinessManager.BusinessMgrInstance.UpdateBooking(Convert.ToInt32(TextBlockBookingId.Text), DatePickerArrivalDate.SelectedDate.Value, DatePickerDepDate.SelectedDate.Value, ConfigurationManager.AppSettings["ConnectionString"]);
            }
            else
            {
                MessageBox.Show("Please select dates");
                return;
            }
            if (ret)
            {
                MessageBox.Show("Saved successfully");
            }
            else
            {
                MessageBox.Show("Error while saving. please try again.");
            }
            this.Close();
        }

        private void DatePickerArrivalDate_OnSelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DatePickerDepDate.DisplayDateStart = DatePickerArrivalDate.SelectedDate;
        }

        private void DatePickerDepDate_OnSelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DatePickerArrivalDate.DisplayDateEnd = DatePickerDepDate.SelectedDate;
        }
    }
}
