﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HolidaysManagement.BL;
using HolidaysManagement.Common.Schema;

namespace HolidayManager
{
    /// <summary>
    /// Interaction logic for EditCustomer.xaml
    /// </summary>
    public partial class EditCustomer : Window
    {
        private ObservableCollection<Customer> _customers = null;
        public EditCustomer()
        {
            InitializeComponent();
            DataGridCustomer.Visibility = Visibility.Collapsed;
            GroupBoxCustomer.Visibility = Visibility.Collapsed;
        }
        //saves the customer details in the database
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxName.Text))
            {
                MessageBox.Show("Please enter name");
                return;
            }
            var returnValue = BusinessManager.BusinessMgrInstance.UpdateCustomer(Convert.ToInt32(TextBlockCustomerId.Text), TextBoxName.Text, TextBoxAddress.Text, ConfigurationManager.AppSettings["ConnectionString"]);
            if (returnValue) MessageBox.Show("Customer details sucessfully updated");
        }
        //edits the row in which button is clicked for customer details
        private void Button_EditClick(object sender, RoutedEventArgs e)
        {
            DataGridCustomer.Visibility = Visibility.Collapsed;
            GroupBoxCustomer.Visibility = Visibility.Visible;
            Customer obj = ((FrameworkElement)sender).DataContext as Customer;
            TextBoxName.Text = obj.CustomerName;
            TextBoxAddress.Text = obj.Address;
            TextBlockCustomerId.Text = Convert.ToString(obj.CustomerReferenceId);
        }
        //deletes the customer row in which button is clicked
        private void Button_DeleteClick(object sender, RoutedEventArgs e)
        {
            Customer obj = ((FrameworkElement)sender).DataContext as Customer;
            if ((obj as Customer).BookingDetails.Count > 0)
            {
                MessageBox.Show("Please delete the bookings before deleting the customer. Please click on edit and it will show all the bookings");
            }
            else
            {
                var returnValue = BusinessManager.BusinessMgrInstance.DeleteCustomer((obj as Customer).CustomerReferenceId, ConfigurationManager.AppSettings["ConnectionString"]);
                _customers.Remove(obj);
            }
        }
        //Search customer button click, will search for the customer similiar to the name in the textbox
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            //Search
            GroupBoxCustomer.Visibility = Visibility.Collapsed;
            var lst = BusinessManager.BusinessMgrInstance.SearchCustomer(TextBoxsearch.Text, ConfigurationManager.AppSettings["ConnectionString"]);
            _customers = new ObservableCollection<Customer>(lst);
            DataGridCustomer.Visibility = Visibility.Visible;
            DataGridCustomer.ItemsSource = _customers;
        }
    }
}
