﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HolidaysManagement.BL;
using HolidaysManagement.Common.Schema;

namespace HolidayManager.Windows
{
    /// <summary>
    /// Interaction logic for AddEditCarHire.xaml
    /// </summary>
    public partial class AddEditCarHire : Window
    {
        private ObservableCollection<Customer> _customers = null;
        private ObservableCollection<Booking> _bookings = null;
        public AddEditCarHire()
        {
            InitializeComponent();
            DataGridBooking.Visibility = Visibility.Collapsed;
            DataGridCustomer.Visibility = Visibility.Collapsed;
            GridCarHire.Visibility = Visibility.Collapsed;
            GroupBoxCarHire.Visibility = Visibility.Collapsed;
        }
        //Search customer button click, will search for the customer similiar to the name in the textbox
        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DataGridCustomer.Visibility = Visibility.Visible;
            DataGridBooking.Visibility = Visibility.Collapsed;
            GridCarHire.Visibility = Visibility.Collapsed;
            GroupBoxCarHire.Visibility = Visibility.Collapsed;
            var lst = BusinessManager.BusinessMgrInstance.SearchCustomer(TextBoxsearch.Text, ConfigurationManager.AppSettings["ConnectionString"]);
            _customers = new ObservableCollection<Customer>(lst);
            DataGridCustomer.Visibility = Visibility.Visible;
            DataGridCustomer.ItemsSource = _customers;
        }
        //this will select the specific customer and will fetch booking details for it in the datagrid
        private void ButtonBase_OnSelectClick(object sender, RoutedEventArgs e)
        {
            DataGridCustomer.Visibility = Visibility.Collapsed;
            DataGridBooking.Visibility = Visibility.Visible;
            Customer obj = ((FrameworkElement)sender).DataContext as Customer;
            _bookings = new ObservableCollection<Booking>(obj.BookingDetails);
            DataGridBooking.ItemsSource = _bookings;
            TextBlockCustomerId.Text = Convert.ToString(obj.CustomerReferenceId);
        }
        //this method will select specific booking and will fetch carhire details for the booking id, and populate in datagrid
        private void ButtonBase_OnSelectBookingClick(object sender, RoutedEventArgs e)
        {
            DataGridCustomer.Visibility = Visibility.Collapsed;
            DataGridBooking.Visibility = Visibility.Collapsed;
            GroupBoxCarHire.Visibility = Visibility.Visible;
            Booking obj = ((FrameworkElement)sender).DataContext as Booking;
            TextblockBookingId.Text = Convert.ToString(obj.BookingReferenceId);
            var bookingDetails = BusinessManager.BusinessMgrInstance.FetchExtrasGuestDetails(Convert.ToInt32(obj.BookingReferenceId), ConfigurationManager.AppSettings["ConnectionString"]);
            if (bookingDetails.CarHireDetails == null)
            {
                CheckBoxCh.IsChecked = false;
            }
            else
            {
                CheckBoxCh.IsChecked = true;
                DatePickerEnd.SelectedDate = bookingDetails.CarHireDetails.EndDate;
                DatePickerStart.SelectedDate = bookingDetails.CarHireDetails.StartDate;
                TextBlockCarHireId.Text = Convert.ToString(bookingDetails.CarHireDetails.CarHireDetailsId);
            }
        }

        private void CheckBoxCh_OnChecked(object sender, RoutedEventArgs e)
        {
            if (CheckBoxCh.IsChecked.Value)
            {
                GridCarHire.Visibility = Visibility.Visible;
            }
            else
            {
                GridCarHire.Visibility = Visibility.Collapsed;
            }
        }
        //saves the car hire details in the databaseS
        private void ButtonBase_OnSaveClick(object sender, RoutedEventArgs e)
        {
            if (CheckBoxCh.IsChecked.Value && DatePickerStart.SelectedDate.HasValue && DatePickerEnd.SelectedDate.HasValue)
            {
                var ret = false;
                if (string.IsNullOrEmpty(TextBlockCarHireId.Text))
                {
                    ret = BusinessManager.BusinessMgrInstance.SaveCarHireDetails(Convert.ToInt32(TextblockBookingId.Text), DatePickerStart.SelectedDate.Value, DatePickerEnd.SelectedDate.Value, ConfigurationManager.AppSettings["ConnectionString"]);
                }
                else
                {
                    ret = BusinessManager.BusinessMgrInstance.UpdateCarHireDetails(Convert.ToInt32(TextBlockCarHireId.Text), DatePickerStart.SelectedDate.Value, DatePickerEnd.SelectedDate.Value, ConfigurationManager.AppSettings["ConnectionString"]);
                }
                if (ret)
                {
                    MessageBox.Show("Saved successfully");
                }
                else
                {
                    MessageBox.Show("Error while saving. please try again.");
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("Please select date");
            }
        }
    }
}
