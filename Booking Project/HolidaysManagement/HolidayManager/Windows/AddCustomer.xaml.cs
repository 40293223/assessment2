﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HolidaysManagement.BL;

namespace HolidayManager.Windows
{
    /// <summary>
    /// Interaction logic for AddCustomer.xaml
    /// </summary>
    public partial class AddCustomer : Window
    {
        public AddCustomer()
        {
            InitializeComponent();
        }
        //saves teh customer details in the database
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxName.Text))
            {
                MessageBox.Show("Please enter name");
                return;
            }
            var custId = BusinessManager.BusinessMgrInstance.RegisterCustomer(TextBoxName.Text, TextBoxAddress.Text, ConfigurationManager.AppSettings["ConnectionString"]);
            if (custId > 0)
            {
                MessageBox.Show("Customer saved successfully");
            }
            this.Close();
        }
    }
}
