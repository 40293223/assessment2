﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HolidaysManagement.BL;
using HolidaysManagement.Common.Schema;

namespace HolidayManager.Windows
{
    /// <summary>
    /// Interaction logic for AddBooking.xaml
    /// </summary>
    public partial class AddBooking : Window
    {
        private ObservableCollection<Customer> _customers = null;
        public AddBooking()
        {
            InitializeComponent();
            DataGridCustomer.Visibility= Visibility.Collapsed;
            GroupBoxBooking.Visibility = Visibility.Collapsed;
        }
        //Search customer button click, will search for the customer similiar to the name in the textbox
        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var lst = BusinessManager.BusinessMgrInstance.SearchCustomer(TextBoxsearch.Text, ConfigurationManager.AppSettings["ConnectionString"]);
            _customers = new ObservableCollection<Customer>(lst);
            DataGridCustomer.Visibility = Visibility.Visible;
            DataGridCustomer.ItemsSource = _customers;
            DataGridCustomer.Visibility = Visibility.Visible;
        }
        //this will select the specific customer and will fetch booking details for it in the datagrid
        private void ButtonBase_SelectOnClick(object sender, RoutedEventArgs e)
        {
            DataGridCustomer.Visibility = Visibility.Collapsed;
            GroupBoxBooking.Visibility = Visibility.Visible;
            Customer obj = ((FrameworkElement)sender).DataContext as Customer;
            TextBlockCustomerId.Text = Convert.ToString(obj.CustomerReferenceId);
        }

        private void DatePickerArrivalDate_OnSelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DatePickerDepDate.DisplayDateStart = DatePickerArrivalDate.SelectedDate;
        }

        private void DatePickerDepDate_OnSelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DatePickerArrivalDate.DisplayDateEnd = DatePickerDepDate.SelectedDate;
        }
        //saves the booking details in the database
        private void ButtonBase_OnSaveClick(object sender, RoutedEventArgs e)
        {
            if (DatePickerDepDate.SelectedDate != null && DatePickerArrivalDate.SelectedDate != null)
            {
                int bookingRefId = BusinessManager.BusinessMgrInstance.InsertBooking(Convert.ToInt32(TextBlockCustomerId.Text), DatePickerArrivalDate.SelectedDate.Value, DatePickerDepDate.SelectedDate.Value, ConfigurationManager.AppSettings["ConnectionString"]);
                this.Close();
            }
            else
            {
                MessageBox.Show("Please select dates");
            }
        }
    }
}
