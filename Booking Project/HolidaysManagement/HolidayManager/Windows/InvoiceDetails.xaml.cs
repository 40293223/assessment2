﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HolidaysManagement.BL;
using HolidaysManagement.Common.Schema;

namespace HolidayManager.Windows
{
    /// <summary>
    /// Interaction logic for InvoiceDetails.xaml
    /// </summary>
    public partial class InvoiceDetails : Window
    {
        private ObservableCollection<Customer> _customers = null;
        private ObservableCollection<Booking> _bookings = null;
        public InvoiceDetails()
        {
            InitializeComponent();
            DataGridBooking.Visibility = Visibility.Collapsed;
            DataGridCustomer.Visibility = Visibility.Collapsed;
        }
        //Search customer button click, will search for the customer similiar to the name in the textbox
        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DataGridCustomer.Visibility = Visibility.Visible;
            DataGridBooking.Visibility = Visibility.Collapsed;
            var lst = BusinessManager.BusinessMgrInstance.SearchCustomer(TextBoxsearch.Text, ConfigurationManager.AppSettings["ConnectionString"]);
            _customers = new ObservableCollection<Customer>(lst);
            DataGridCustomer.Visibility = Visibility.Visible;
            DataGridCustomer.ItemsSource = _customers;
        }
        //this will select the specific customer and will fetch booking details for it in the datagrid
        private void ButtonBase_OnSelectClick(object sender, RoutedEventArgs e)
        {
            Customer obj = ((FrameworkElement)sender).DataContext as Customer;
            _bookings = new ObservableCollection<Booking>(obj.BookingDetails);
            if (_bookings.Count > 0)
            {
                DataGridCustomer.Visibility = Visibility.Collapsed;
                DataGridBooking.Visibility = Visibility.Visible;
                DataGridBooking.ItemsSource = _bookings;
                TextBlockCustomerId.Text = Convert.ToString(obj.CustomerReferenceId);
            }
            else
            {
                MessageBox.Show("No bookings for this customer. please add booking first.");
            }
        }
        //Open generateinvoice window, when selecte the booking row's generate invoice button
        private void ButtonBase_OnSelectBookingClick(object sender, RoutedEventArgs e)
        {
            Booking obj = ((FrameworkElement)sender).DataContext as Booking;
            var invoice = BusinessManager.BusinessMgrInstance.GenerateInvoice(obj.BookingReferenceId, ConfigurationManager.AppSettings["ConnectionString"]);
            InvoiceWindow iv = new InvoiceWindow(invoice);
            iv.ShowInTaskbar = false;
            iv.ShowDialog();
        }
    }
}
