﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidaysManagement.DAL
{
    public class DatalayerManager
    {
        private static DatalayerManager _instance = null;

        public static DatalayerManager DataLayerInstance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new DatalayerManager();
                }
                return _instance;
            }
        }

        private SqlParameter GetSqlParameter(string parameterName, SqlDbType type, object value, ParameterDirection direction)
        {
            SqlParameter parm = new SqlParameter();
            parm.ParameterName = parameterName;
            parm.SqlDbType = type;
            if (value != null)
            {
                parm.Value = value;
            }
            else
            {
                parm.Value = System.DBNull.Value;
            }
            parm.Direction = direction;
            return parm;
        }

        public DataSet SearchBookingDetails(string customerName, string connectionString)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"SELECT * FROM [CustomerDetails] WHERE CustomerName LIKE '%'+@CustomerName+'%';SELECT * FROM BookingDetails bk JOIN CustomerDetails ck ON bk.CustomerReferenceID = ck.CustomerReferenceID WHERE ck.CustomerName LIKE '%'+@CustomerName+'%'";
                        command.Parameters.Add(GetSqlParameter("@CustomerName", SqlDbType.VarChar, customerName, ParameterDirection.Input));
                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return ds;
        }

        public int RegisterCustomer(string customerName, string customerAddress, string connectionString)
        {
            int customerId = 0;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"INSERT INTO [CustomerDetails] (CustomerName, [Address]) VALUES (@CustomerName, @CustomerAddress);SELECT SCOPE_IDENTITY()";
                        command.Parameters.Add(GetSqlParameter("@CustomerName", SqlDbType.VarChar, customerName, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@CustomerAddress", SqlDbType.VarChar, customerAddress, ParameterDirection.Input));
                        customerId = Convert.ToInt32(command.ExecuteScalar());
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return customerId;
        }

        public bool UpdateCustomer(int customerId, string customerName, string customerAddress, string connectionString)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"UPDATE [CustomerDetails] SET CustomerName = @CustomerName, [Address] = @CustomerAddress WHERE CustomerReferenceID = @CustomerReferenceID";
                        command.Parameters.Add(GetSqlParameter("@CustomerName", SqlDbType.VarChar, customerName, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@CustomerAddress", SqlDbType.VarChar, customerAddress, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@CustomerReferenceID", SqlDbType.Int, customerId, ParameterDirection.Input));
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return true;
        }

        public int InsertBooking(int customerId, DateTime arrivalDate, DateTime departureDate, string connectionString)
        {
            int bookingId = 0;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"INSERT INTO [BookingDetails] (CustomerReferenceID, ArrivalDate, DepartureDate) VALUES (@CustomerReferenceID, @ArrivalDate, @DepartureDate);SELECT SCOPE_IDENTITY()";
                        command.Parameters.Add(GetSqlParameter("@ArrivalDate", SqlDbType.DateTime, arrivalDate, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@DepartureDate", SqlDbType.DateTime, departureDate, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@CustomerReferenceID", SqlDbType.Int, customerId, ParameterDirection.Input));
                        bookingId = Convert.ToInt32(command.ExecuteScalar());
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return bookingId;
        }

        public bool UpdateBooking(int bookingId, DateTime arrivalDate, DateTime departureDate, string connectionString)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"UPDATE [BookingDetails] SET ArrivalDate = @ArrivalDate, DepartureDate = @DepartureDate WHERE BookingReferenceID = @BookingReferenceID";
                        command.Parameters.Add(GetSqlParameter("@ArrivalDate", SqlDbType.DateTime, arrivalDate, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@DepartureDate", SqlDbType.DateTime, departureDate, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@BookingReferenceID", SqlDbType.Int, bookingId, ParameterDirection.Input));
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return true;
        }

        public DataSet FetchBookingDetails(int customerId, string connectionString)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"SELECT * FROM [BookingDetails] WHERE CustomerReferenceID = @CustomerReferenceID";
                        command.Parameters.Add(GetSqlParameter("@CustomerReferenceID", SqlDbType.Int, customerId, ParameterDirection.Input));
                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return ds;
        }

        public DataSet FetchExtrasGuestDetails(int bookingId, string connectionString)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"SELECT * FROM BookingDetails WHERE BookingReferenceID = @BookingReferenceID;
SELECT* FROM[BookingGuestDetails] WHERE BookingReferenceID = @BookingReferenceID;
SELECT* FROM[BookingExtrasDetails] WHERE BookingReferenceID = @BookingReferenceID;
SELECT* FROM[BookingCarHireDetails] WHERE BookingReferenceID = @BookingReferenceID";
                        command.Parameters.Add(GetSqlParameter("@BookingReferenceID", SqlDbType.Int, bookingId, ParameterDirection.Input));
                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return ds;
        }
        public DataSet FetchGuestDetails(int bookingId, string connectionString)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"SELECT * FROM [BookingGuestDetails]	WHERE BookingReferenceID = @BookingReferenceID";
                        command.Parameters.Add(GetSqlParameter("@BookingReferenceID", SqlDbType.Int, bookingId, ParameterDirection.Input));
                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return ds;
        }

        public bool InsertGuest(int bookingId, string name, string passportNumber, int age, string connectionString)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"INSERT INTO [BookingGuestDetails] (BookingReferenceID, Name, PassportNumber, Age) VALUES (@BookingReferenceID, @Name, @PassportNumber, @Age)";
                        command.Parameters.Add(GetSqlParameter("@BookingReferenceID", SqlDbType.Int, bookingId, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@Name", SqlDbType.VarChar, name, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@PassportNumber", SqlDbType.VarChar, passportNumber, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@Age", SqlDbType.Int, age, ParameterDirection.Input));
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return true;
        }

        public bool UpdateGuest(int bookingGuestId, string name, string passportNumber, int age, string connectionString)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"UPDATE [BookingGuestDetails] SET Name = @Name, PassportNumber = @PassportNumber, Age = @Age WHERE BookingGuestID = @BookingGuestID";
                        command.Parameters.Add(GetSqlParameter("@BookingGuestID", SqlDbType.Int, bookingGuestId, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@Name", SqlDbType.VarChar, name, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@PassportNumber", SqlDbType.VarChar, passportNumber, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@Age", SqlDbType.Int, age, ParameterDirection.Input));
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return true;
        }

        public bool InsertExtras(int bookingId, bool isSameAlldays, DateTime? dates, bool hasOptedForbf, string bfReq, bool hasOptedforEve, string eveReq, string connectionString)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"INSERT INTO [BookingExtrasDetails] ([BookingReferenceID], [HasOptedSameForAllDays], [Date], [HasOptedForBreakfast], [BreakfastRequirements], [HasOptedForEveningMeal], [EveningMealRequirements]) VALUES(@BookingReferenceID, @SameForAllDays, @Date, @BfOption, @BfReq, @EveOption, @EveReq)";
                        command.Parameters.Add(GetSqlParameter("@BookingReferenceID", SqlDbType.Int, bookingId, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@SameForAllDays", SqlDbType.Bit, isSameAlldays, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@Date", SqlDbType.DateTime, dates, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@BfOption", SqlDbType.Bit, hasOptedForbf, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@BfReq", SqlDbType.VarChar, bfReq, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@EveOption", SqlDbType.Bit, hasOptedforEve, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@EveReq", SqlDbType.VarChar, eveReq, ParameterDirection.Input));
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return true;
        }

        public bool UpdateExtras(int extrasId, bool isSameAlldays, DateTime? dates, bool hasOptedForbf, string bfReq, bool hasOptedforEve, string eveReq, string connectionString)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"UPDATE [BookingExtrasDetails] SET [HasOptedSameForAllDays] = @SameForAllDays, [Date] = @Date, [HasOptedForBreakfast] = @BfOption, [BreakfastRequirements] = @BfReq, [HasOptedForEveningMeal] = @EveOption, [EveningMealRequirements] = @EveReq WHERE BookingExtrasDetailsID = @BookingExtrasID";
                        command.Parameters.Add(GetSqlParameter("@BookingExtrasID", SqlDbType.Int, extrasId, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@SameForAllDays", SqlDbType.Bit, isSameAlldays, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@Date", SqlDbType.DateTime, dates, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@BfOption", SqlDbType.Bit, hasOptedForbf, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@BfReq", SqlDbType.VarChar, bfReq, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@EveOption", SqlDbType.Bit, hasOptedforEve, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@EveReq", SqlDbType.VarChar, eveReq, ParameterDirection.Input));
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return true;
        }

        public bool DeleteExtras(int bookingExtrasid, string connectionString)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"DELETE FROM [BookingExtrasDetails] WHERE BookingExtrasDetailsID = @BookingExtrasID";
                        command.Parameters.Add(GetSqlParameter("@BookingExtrasID", SqlDbType.Int, bookingExtrasid, ParameterDirection.Input));
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return true;
        }

        public bool DeleteBooking(int bookingid, string connectionString)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"DELETE FROM [BookingExtrasDetails] WHERE BookingReferenceID = @BookingReferenceID;
DELETE FROM BookingCarHireDetails WHERE BookingReferenceID = @BookingReferenceID;
DELETE FROM BookingGuestDetails WHERE BookingReferenceID = @BookingReferenceID;
DELETE FROM BookingDetails WHERE BookingReferenceID = @BookingReferenceID";
                        command.Parameters.Add(GetSqlParameter("@BookingReferenceID", SqlDbType.Int, bookingid, ParameterDirection.Input));
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return true;
        }

        public object DeleteCustomer(int customerReferenceId, string connectionString)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"DELETE FROM CustomerDetails WHERE CustomerReferenceID = @CustomerReferenceID";
                        command.Parameters.Add(GetSqlParameter("@CustomerReferenceID", SqlDbType.Int, customerReferenceId, ParameterDirection.Input));
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return true;
        }

        public bool SaveCarHireDetails(int bookingId, string driverName, DateTime startDate, DateTime endDate, string connectionString)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"INSERT INTO BookingCarHireDetails (BookingReferenceID, Days, DriverName, StartDate, EndDate)VALUES(@BookingReferenceID, @Days, @DriverName, @StartDate, @EndDate)";
                        command.Parameters.Add(GetSqlParameter("@BookingReferenceID", SqlDbType.Int, bookingId, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@Days", SqlDbType.Int, Convert.ToInt32((endDate - startDate).TotalDays), ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@StartDate", SqlDbType.DateTime, startDate, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@EndDate", SqlDbType.DateTime, endDate, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@DriverName", SqlDbType.VarChar, driverName, ParameterDirection.Input));
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return true;
        }

        public DataSet FetchBookingDetailsForInvoice(int bookingId, string connectionString)
        {
            DataSet ds = new DataSet();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"DECLARE @CustomerID INT 
                                SELECT @CustomerID = CustomerReferenceID FROM BookingDetails WHERE BookingReferenceID = @BookingReferenceID;
                                SELECT* FROM CustomerDetails WHERE CustomerReferenceID = @CustomerID;
                                SELECT* FROM BookingDetails WHERE BookingReferenceID = @BookingReferenceID;
                                SELECT* FROM BookingExtrasDetails WHERE BookingReferenceID = @BookingReferenceID;
                                SELECT* FROM BookingGuestDetails WHERE BookingReferenceID = @BookingReferenceID;
                                SELECT* FROM BookingCarHireDetails WHERE BookingReferenceID = @BookingReferenceID";
                        command.Parameters.Add(GetSqlParameter("@BookingReferenceID", SqlDbType.Int, bookingId, ParameterDirection.Input));
                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.Fill(ds);
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return ds;
        }

        public bool DeleteGuest(int guestBookingGuestId, string connectionString)
        {
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = @"DELETE FROM BookingGuestDetails WHERE BookingGuestID = @BookingGuestID";
                        command.Parameters.Add(GetSqlParameter("@BookingGuestID", SqlDbType.Int, guestBookingGuestId, ParameterDirection.Input));
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return true;
        }

        public bool UpdateCarHireDetails(int carHiretoInt32, DateTime selectedstartDateValue, DateTime endvalue, string appSetting, string driverName)
        {
            using (SqlConnection conn = new SqlConnection(appSetting))
            {
                using (SqlCommand command = new SqlCommand())
                {
                    try
                    {
                        conn.Open();
                        command.Connection = conn;
                        command.CommandType = CommandType.Text;
                        command.CommandText = "UPDATE BookingCarHireDetails SET Days = @Days, DriverName = @DriverName, StartDate = @StartDate, EndDate = @EndDate WHERE CarHireDetailsDetailsID =  @CarHireID";
                        command.Parameters.Add(GetSqlParameter("@Days", SqlDbType.Int, Convert.ToInt32((endvalue - selectedstartDateValue).TotalDays), ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@DriverName", SqlDbType.VarChar, driverName, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@StartDate", SqlDbType.DateTime, selectedstartDateValue, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@EndDate", SqlDbType.DateTime, endvalue, ParameterDirection.Input));
                        command.Parameters.Add(GetSqlParameter("@CarHireID", SqlDbType.Int, carHiretoInt32, ParameterDirection.Input));
                        command.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        if (conn.State != ConnectionState.Closed)
                        {
                            conn.Close();
                        }
                    }
                }
            }
            return true;
        }
    }
}
