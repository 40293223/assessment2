﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidaysManagement.Common.Schema
{
    public class Invoice
    {
        public int BookingReferenceId { get; set; }
        public int TotalBasicCost { get; set; }
        public int NoOfDaysInBooking { get; set; }
        public int EveningMealCost { get; set; }
        public int BreakfastCost { get; set; }
        public int CarHireCost { get; set; }

        public int TotalCost
        {
            get { return TotalBasicCost + EveningMealCost + BreakfastCost + CarHireCost; }
        }

    }
}
