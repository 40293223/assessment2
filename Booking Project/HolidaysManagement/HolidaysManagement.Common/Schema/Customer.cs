﻿using System.Collections.Generic;

namespace HolidaysManagement.Common.Schema
{
    public class Customer
    {
        public int CustomerReferenceId { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public List<Booking> BookingDetails { get; set; }
    }
}
