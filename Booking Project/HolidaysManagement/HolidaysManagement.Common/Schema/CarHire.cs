﻿using System;

namespace HolidaysManagement.Common.Schema
{
    public class CarHire
    {
        public int CarHireDetailsId { get; set; }
        public int BookingReferenceId { get; set; }
        public int Days { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string DriverName { get; set; }
    }
}
