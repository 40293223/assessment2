﻿using System;
using System.Collections.Generic;

namespace HolidaysManagement.Common.Schema
{
    public class Booking
    {
        public int BookingReferenceId { get; set; }
        public int CustomerReferenceId { get; set; }
        public DateTime ArrivalDate { get; set; }
        public DateTime DepartureDate { get; set; }

        private List<Guest> _guestList = new List<Guest>();

        public List<Guest> GuestList
        {
            get { return _guestList; }
            set { _guestList = value; }
        }
        private List<Extras> _extrasList = new List<Extras>();

        public List<Extras> ExtrasList
        {
            get { return _extrasList; }
            set { _extrasList = value; }
        }

        public CarHire CarHireDetails { get; set; }
    }
}
