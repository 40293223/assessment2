﻿namespace HolidaysManagement.Common.Schema
{
    public class Guest
    {
        public int BookingGuestId { get; set; }
        public int BookingReferenceId { get; set; }
        public string Name { get; set; }
        public string PassportNumber { get; set; }
        public int Age { get; set; }

    }
}
