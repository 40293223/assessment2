﻿using System;

namespace HolidaysManagement.Common.Schema
{
    public class Extras
    {
        public int BookingExtrasDetailsId { get; set; }
        public int BookingReferenceId { get; set; }
        public bool HasOptedSameForAllDays { get; set; }
        public DateTime? Date { get; set; }
        public bool HasOptedForBreakfast { get; set; }
        public string BreakfastRequirements { get; set; }
        public bool HasOptedForEveningMeal { get; set; }
        public string EveningMealRequirements { get; set; }
    }
}
