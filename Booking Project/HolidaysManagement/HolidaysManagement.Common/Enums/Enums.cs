﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HolidaysManagement.Common.Enums
{
    public enum ExtrasSelectionEnum
    {
        OptSameForAllDays,
        SelectDifferentForEachDay
    }

    public enum CreateControl
    {
        AddCustomerBooking,
        EditCustomerBooking,
        DeleteCustomer,
        GenerateInvoice
    }
}
