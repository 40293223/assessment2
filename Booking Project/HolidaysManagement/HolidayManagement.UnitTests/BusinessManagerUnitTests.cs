﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HolidaysManagement.BL;
using HolidaysManagement.Common.Schema;
using NUnit.Framework;

namespace HolidayManagement.UnitTests
{
    [TestFixture]
    public class BusinessManagerUnitTests
    {
        [Test]
        public void CalculateCarHireCost()
        {
            CarHire carHire = new CarHire()
            {
                Days = 3
            };
            int cost = BusinessManager.BusinessMgrInstance.CalculateCarHireCost(carHire);
            Assert.AreEqual(150, cost);
        }

        [Test]
        public void CalculateBasicCost()
        {
            List<Guest> guestLst = new List<Guest>();
            guestLst.Add(new Guest() {Age = 23});
            guestLst.Add(new Guest() {Age = 15});
            int cost = BusinessManager.BusinessMgrInstance.CalculateBasicCost(guestLst);
            Assert.AreEqual(80, cost);
        }

        [Test]
        public void CalculateBreakfastCost()
        {
            List<Extras> guestLst = new List<Extras>();
            guestLst.Add(new Extras() { HasOptedSameForAllDays = false, HasOptedForBreakfast = true});
            guestLst.Add(new Extras() { HasOptedSameForAllDays = false, HasOptedForBreakfast = true });
            int cost = BusinessManager.BusinessMgrInstance.CalculateBreakfastCost(guestLst, 2, 2);
            Assert.AreEqual(20, cost);
        }

        [Test]
        public void CalculateBreakfastCostSame()
        {
            List<Extras> guestLst = new List<Extras>();
            guestLst.Add(new Extras() { HasOptedSameForAllDays = true, HasOptedForBreakfast = true });
            guestLst.Add(new Extras() { HasOptedSameForAllDays = true, HasOptedForBreakfast = true });
            int cost = BusinessManager.BusinessMgrInstance.CalculateBreakfastCost(guestLst, 3, 2);
            Assert.AreEqual(30, cost);
        }

        [Test]
        public void CalculateEveningMealCost()
        {
            List<Extras> guestLst = new List<Extras>();
            guestLst.Add(new Extras() { HasOptedSameForAllDays = false, HasOptedForEveningMeal = true });
            guestLst.Add(new Extras() { HasOptedSameForAllDays = false, HasOptedForEveningMeal = true });
            int cost = BusinessManager.BusinessMgrInstance.CalculateEveningMealCost(guestLst, 2, 2);
            Assert.AreEqual(60, cost);
        }

    }
}
