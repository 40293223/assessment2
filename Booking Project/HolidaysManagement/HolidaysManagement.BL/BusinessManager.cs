﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HolidaysManagement.Common.Schema;
using HolidaysManagement.DAL;

namespace HolidaysManagement.BL
{
    public class BusinessManager
    {
        //using singleton patteren
        private static BusinessManager _instance;

        public static BusinessManager BusinessMgrInstance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new BusinessManager();
                }
                return _instance;
            }
        }

        //This method will search customers in database with similiar customer name string
        public List<Customer> SearchCustomer(string customerName, string connectionString)
        {
            List<Customer> customerDetails = new List<Customer>();
            try
            {
                DataSet ds = DatalayerManager.DataLayerInstance.SearchBookingDetails(customerName, connectionString);
                if (ds != null && ds.Tables.Count > 0)
                {
                    List<Booking> bookingDetails = new List<Booking>();
                    if (ds.Tables.Count > 1)
                    {
                        foreach (DataRow dataRow in ds.Tables[1].Rows)
                        {
                            bookingDetails.Add(new Booking() {BookingReferenceId = Convert.ToInt32(dataRow["BookingReferenceID"]), ArrivalDate = DateTime.Parse(Convert.ToString(dataRow["ArrivalDate"])), DepartureDate = DateTime.Parse(Convert.ToString(dataRow["DepartureDate"])), CustomerReferenceId = Convert.ToInt32(dataRow["CustomerReferenceID"])});
                        }
                    }
                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow dataRow in ds.Tables[0].Rows)
                        {
                            customerDetails.Add(new Customer() {Address = Convert.ToString(dataRow["Address"]), CustomerReferenceId = Convert.ToInt32(dataRow["CustomerReferenceID"]), CustomerName = Convert.ToString(dataRow["CustomerName"]), BookingDetails = bookingDetails.Where(x => x.CustomerReferenceId == Convert.ToInt32(dataRow["CustomerReferenceID"])).ToList()});
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return customerDetails;
        }

        //this will handle exception and write in eventlog
        private void WriteEventLog(Exception exception)
        {
            try
            {
                EventLog.WriteEntry("Application", exception.ToString());
            }
            catch (Exception)
            {
                //Run as Admin
            }
        }

        //this method will register customer in the database
        public int RegisterCustomer(string customerName, string customerAddress, string connectionString)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.RegisterCustomer(customerName, customerAddress, connectionString);
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return 0;
        }

        //this method will update the customer in the database
        public bool UpdateCustomer(int customerId, string customerName, string customerAddress, string connectionString)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.UpdateCustomer(customerId, customerName, customerAddress, connectionString);
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return false;
        }

        //this method will insert booking details in the database
        public int InsertBooking(int customerId, DateTime arrivalDate, DateTime departureDate, string connectionString)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.InsertBooking(customerId, arrivalDate, departureDate, connectionString);
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return 0;
        }

        //this method will update booking details in the database
        public bool UpdateBooking(int bookingId, DateTime arrivalDate, DateTime departureDate, string connectionString)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.UpdateBooking(bookingId, arrivalDate, departureDate, connectionString);
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return false;
        }

        //this method will insert guest details in the database
        public bool InsertGuest(int bookingId, string name, string passportNumber, int age, string connectionString)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.InsertGuest(bookingId, name, passportNumber, age, connectionString);
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return false;
        }

        //this method will update guest details in the database
        public bool UpdateGuest(int bookingGuestId, string name, string passportNumber, int age, string connectionString)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.UpdateGuest(bookingGuestId, name, passportNumber, age, connectionString);
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return false;
        }

        //this method will fetch all the extras, guest, car hire details for specific booking id
        public Booking FetchExtrasGuestDetails(int bookingId, string connectionString)
        {
            Booking bookDetails = new Booking();
            try
            {
                var ds = DatalayerManager.DataLayerInstance.FetchExtrasGuestDetails(bookingId, connectionString);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables.Count > 3)
                    {
                        List<CarHire> carhire = new List<CarHire>();
                        foreach (DataRow row in ds.Tables[3].Rows)
                        {
                            carhire.Add(new CarHire() { BookingReferenceId = Convert.ToInt32(row["BookingReferenceID"]), CarHireDetailsId = Convert.ToInt32(row["CarHireDetailsDetailsID"]), Days = Convert.ToInt32(row["Days"]), StartDate = DateTime.Parse(Convert.ToString(row["StartDate"])), EndDate = DateTime.Parse(Convert.ToString(row["EndDate"])), DriverName = Convert.ToString(row["DriverName"]) });
                        }
                        bookDetails.CarHireDetails = carhire.FirstOrDefault();
                    }
                    if (ds.Tables.Count > 2)
                    {
                        List<Extras> extras = new List<Extras>();
                        foreach (DataRow row in ds.Tables[2].Rows)
                        {
                            extras.Add(new Extras()
                            {
                                BookingReferenceId = Convert.ToInt32(row["BookingReferenceID"]),
                                BookingExtrasDetailsId = Convert.ToInt32(row["BookingExtrasDetailsID"]),
                                HasOptedForBreakfast = Convert.ToBoolean(row["HasOptedForBreakfast"]),
                                HasOptedForEveningMeal = Convert.ToBoolean((row["HasOptedForEveningMeal"])),
                                HasOptedSameForAllDays = Convert.ToBoolean((row["HasOptedSameForAllDays"])),
                                BreakfastRequirements = Convert.ToString(row["BreakfastRequirements"]),
                                EveningMealRequirements = Convert.ToString(row["EveningMealRequirements"]),
                                Date = row["Date"] is DBNull ? (DateTime?)null : DateTime.Parse(Convert.ToString(row["Date"]))
                            });
                        }
                        bookDetails.ExtrasList = extras;
                    }
                    if (ds.Tables.Count > 1)
                    {
                        List<Guest> extras = new List<Guest>();
                        foreach (DataRow row in ds.Tables[1].Rows)
                        {
                            extras.Add(new Guest() { BookingReferenceId = Convert.ToInt32(row["BookingReferenceID"]), BookingGuestId = Convert.ToInt32(row["BookingGuestID"]), Name = Convert.ToString(row["Name"]), PassportNumber = Convert.ToString(Convert.ToString(row["PassportNumber"])), Age = Convert.ToInt32((row["Age"])) });
                        }
                        bookDetails.GuestList = extras;
                    }
                    if (ds.Tables.Count > 0)
                    {
                        bookDetails.CustomerReferenceId = Convert.ToInt32(ds.Tables[0].Rows[0]["CustomerReferenceID"]);
                        bookDetails.BookingReferenceId = Convert.ToInt32(ds.Tables[0].Rows[0]["BookingReferenceID"]);
                        bookDetails.DepartureDate = DateTime.Parse(Convert.ToString(ds.Tables[0].Rows[0]["DepartureDate"]));
                        bookDetails.ArrivalDate = DateTime.Parse(Convert.ToString(ds.Tables[0].Rows[0]["ArrivalDate"]));
                    }
                }
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return bookDetails;
        }

        //this method will all the bookings for the specific customerid
        public List<Booking> FetchBookingDetails(int customerId, string connectionString)
        {
            List<Booking> bookDetails = new List<Booking>();
            try
            {
                var ds = DatalayerManager.DataLayerInstance.FetchBookingDetails(customerId, connectionString);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            bookDetails.Add(new Booking() { BookingReferenceId = Convert.ToInt32(row["BookingReferenceID"]), ArrivalDate = DateTime.Parse(Convert.ToString(row["ArrivalDate"])), DepartureDate = DateTime.Parse(Convert.ToString(row["DepartureDate"])), CustomerReferenceId = Convert.ToInt32(row["CustomerReferenceID"]) });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return bookDetails;
        }

        //this method will fetch guest details for the specific booking id
        public List<Guest> FetchGuestDetails(int bookingId, string connectionString)
        {
            List<Guest> bookDetails = new List<Guest>();
            try
            {
                var ds = DatalayerManager.DataLayerInstance.FetchGuestDetails(bookingId, connectionString);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            bookDetails.Add(new Guest() { BookingGuestId = Convert.ToInt32(row["BookingGuestID"]), BookingReferenceId = Convert.ToInt32(row["BookingReferenceID"]), Name = Convert.ToString(row["Name"]), PassportNumber = Convert.ToString(row["PassportNumber"]), Age = Convert.ToInt32(row["Age"]) });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return bookDetails;
        }
        //this method will insert extras details in the database
        public bool InsertExtras(int bookingId, bool isSameAlldays, DateTime? dates, bool hasOptedForbf, string bfReq, bool hasOptedforEve, string eveReq, string connectionString)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.InsertExtras(bookingId, isSameAlldays, dates, hasOptedForbf, bfReq, hasOptedforEve, eveReq, connectionString);

            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return false;
        }
        //this method will delete extras details from the database
        public bool DeleteExtras(int bookingExtrasid, string connectionString)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.DeleteExtras(bookingExtrasid, connectionString);
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return false;
        }
        //this method will delete booking details from the database
        public bool DeleteBooking(int bookingid, string connectionString)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.DeleteBooking(bookingid, connectionString);
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return false;
        }
        //this method will update extras details in the database
        public bool UpdateExtras(int extrasId, bool isSameAlldays, DateTime? dates, bool hasOptedForbf, string bfReq, bool hasOptedforEve, string eveReq, string connectionString)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.UpdateExtras(extrasId, isSameAlldays, dates, hasOptedForbf, bfReq, hasOptedforEve, eveReq, connectionString);
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return false;
        }
        //this method will delete customer details from the database
        public object DeleteCustomer(int customerReferenceId, string connectionString)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.DeleteCustomer(customerReferenceId, connectionString);
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return false;
        }
        //this method will insert carhire details in the database
        public bool SaveCarHireDetails(int bookingId, DateTime startDate, DateTime endDate, string connectionString)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.SaveCarHireDetails(bookingId, SearchFreeDriver(), startDate, endDate, connectionString);
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return false;
        }
        //this method will return driver name string
        private string SearchFreeDriver()
        {
            return "Joey";
        }
        //this method will fetch booking details for the specific booking id fro the invoice generation
        public Customer FetchBookingDetailsForInvoice(int bookingId, string connectionString)
        {
            Customer customerDetail = new Customer();
            try
            {
                Booking bookDetails = new Booking();
                var ds = DatalayerManager.DataLayerInstance.FetchBookingDetailsForInvoice(bookingId, connectionString);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables.Count > 4)
                    {
                        List<CarHire> carhire = new List<CarHire>();
                        foreach (DataRow row in ds.Tables[4].Rows)
                        {
                            carhire.Add(new CarHire() {BookingReferenceId = Convert.ToInt32(row["BookingReferenceID"]), CarHireDetailsId = Convert.ToInt32(row["CarHireDetailsDetailsID"]), Days = Convert.ToInt32(row["Days"]), StartDate = DateTime.Parse(Convert.ToString(row["StartDate"])), EndDate = DateTime.Parse(Convert.ToString(row["EndDate"])), DriverName = Convert.ToString(row["DriverName"])});
                        }
                        bookDetails.CarHireDetails = carhire.FirstOrDefault();
                    }
                    if (ds.Tables.Count > 3)
                    {
                        List<Guest> extras = new List<Guest>();
                        foreach (DataRow row in ds.Tables[3].Rows)
                        {
                            extras.Add(new Guest() {BookingReferenceId = Convert.ToInt32(row["BookingReferenceID"]), BookingGuestId = Convert.ToInt32(row["BookingGuestID"]), Name = Convert.ToString(row["Name"]), PassportNumber = Convert.ToString(Convert.ToString(row["PassportNumber"])), Age = Convert.ToInt32((row["Age"]))});
                        }
                        bookDetails.GuestList = extras;
                    }
                    if (ds.Tables.Count > 2)
                    {
                        List<Extras> extras = new List<Extras>();
                        foreach (DataRow row in ds.Tables[2].Rows)
                        {
                            extras.Add(new Extras() {BookingReferenceId = Convert.ToInt32(row["BookingReferenceID"]), BookingExtrasDetailsId = Convert.ToInt32(row["BookingExtrasDetailsID"]), HasOptedForBreakfast = Convert.ToBoolean(row["HasOptedForBreakfast"]), HasOptedForEveningMeal = Convert.ToBoolean((row["HasOptedForEveningMeal"])), HasOptedSameForAllDays = Convert.ToBoolean((row["HasOptedSameForAllDays"])), BreakfastRequirements = Convert.ToString(row["BreakfastRequirements"]), EveningMealRequirements = Convert.ToString(row["EveningMealRequirements"]),});
                        }
                        bookDetails.ExtrasList = extras;
                    }
                    if (ds.Tables.Count > 1)
                    {
                        bookDetails.CustomerReferenceId = Convert.ToInt32(ds.Tables[1].Rows[0]["CustomerReferenceID"]);
                        bookDetails.BookingReferenceId = Convert.ToInt32(ds.Tables[1].Rows[0]["BookingReferenceID"]);
                        bookDetails.DepartureDate = DateTime.Parse(Convert.ToString(ds.Tables[1].Rows[0]["DepartureDate"]));
                        bookDetails.ArrivalDate = DateTime.Parse(Convert.ToString(ds.Tables[1].Rows[0]["ArrivalDate"]));
                    }
                    if (ds.Tables.Count > 0)
                    {
                        customerDetail.CustomerReferenceId = Convert.ToInt32(ds.Tables[0].Rows[0]["CustomerReferenceID"]);
                        customerDetail.CustomerName = Convert.ToString(ds.Tables[0].Rows[0]["CustomerName"]);
                        customerDetail.Address = Convert.ToString(ds.Tables[0].Rows[0]["Address"]);
                    }
                    var lst = new List<Booking>();
                    lst.Add(bookDetails);
                    customerDetail.BookingDetails = lst;
                }
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return customerDetail;
        }
        //this method will generate invoice details
        public Invoice GenerateInvoice(int bookingReferenceId, string connectionstring)
        {
            try
            {
                Invoice invoice = new Invoice();
                Customer csDetails = FetchBookingDetailsForInvoice(bookingReferenceId, connectionstring);
                invoice = GenerateInvoice(csDetails);
                return invoice;
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return null;
        }

        private Invoice GenerateInvoice(Customer csDetails)
        {
            Invoice cs = new Invoice();
            var booking = csDetails.BookingDetails.FirstOrDefault();
            if (booking != null)
            {
                int noOfGuests = 0;
                if (booking.GuestList != null)
                {
                    noOfGuests = booking.GuestList.Count;
                }
                cs.BookingReferenceId = booking.BookingReferenceId;
                cs.NoOfDaysInBooking = Convert.ToInt32((booking.DepartureDate - booking.ArrivalDate).TotalDays);
                //calculate basic cost based on guestlist
                cs.TotalBasicCost = CalculateBasicCost(booking.GuestList);
                cs.EveningMealCost = CalculateEveningMealCost(booking.ExtrasList, Convert.ToInt32((booking.DepartureDate - booking.ArrivalDate).TotalDays), noOfGuests);
                cs.BreakfastCost = CalculateBreakfastCost(booking.ExtrasList, Convert.ToInt32((booking.DepartureDate - booking.ArrivalDate).TotalDays), noOfGuests);
                cs.CarHireCost = CalculateCarHireCost(booking.CarHireDetails);

            }
            return cs;
        }
        //this method will calculate car hire cost
        public int CalculateCarHireCost(CarHire bookingCarHireDetails)
        {
            int carHireCost = 0;
            if (bookingCarHireDetails != null)
            {
                carHireCost = 50*(bookingCarHireDetails.Days);
            }
            return carHireCost;
        }
        //this method will calculat evening meal cost
        public int CalculateEveningMealCost(List<Extras> bookingExtrasList, int days, int noOfGuest)
        {
            int eveMealCost = 0;
            foreach (var extras in bookingExtrasList)
            {
                if (extras.HasOptedForEveningMeal)
                {
                    if (extras.HasOptedSameForAllDays)
                    {
                        eveMealCost = 15 * days * noOfGuest;
                        break;
                    }
                    eveMealCost = eveMealCost + (15 * noOfGuest);
                }
                
            }
            return eveMealCost;
        }

        //this method will calculate breakfast cost
        public int CalculateBreakfastCost(List<Extras> bookingExtrasList, int days, int noOfGuest)
        {
            int breakFastCost = 0;
            foreach (var extras in bookingExtrasList)
            {
                if (extras.HasOptedForBreakfast)
                {
                    if (extras.HasOptedSameForAllDays)
                    {
                        breakFastCost = 5 * days * noOfGuest;
                        break;
                    }
                    breakFastCost = breakFastCost + (5 * noOfGuest) ;
                }

            }
            return breakFastCost;
        }
        //this method will calculate basic cost
        public int CalculateBasicCost(List<Guest> bookingGuestList)
        {
            int totalCost = 0;
            foreach (var guest in bookingGuestList)
            {
                if (guest.Age < 18)
                {
                    totalCost = totalCost + 30;
                }
                else
                {
                    totalCost = totalCost + 50;
                }
            }
            return totalCost;
        }
        //this method will delete guest details from the database
        public bool DeleteGuest(int guestBookingGuestId, string connectionString)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.DeleteGuest(guestBookingGuestId, connectionString);
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return false;
        }

        //this method will update car hrie details in the database
        public bool UpdateCarHireDetails(int carHiretoInt32, DateTime selectedstartDateValue, DateTime endvalue, string appSetting)
        {
            try
            {
                return DatalayerManager.DataLayerInstance.UpdateCarHireDetails(carHiretoInt32, selectedstartDateValue, endvalue, appSetting, SearchFreeDriver());
            }
            catch (Exception ex)
            {
                WriteEventLog(ex);
            }
            return false;
        }
    }
}
