CREATE TABLE [dbo].[CustomerDetails](
	[CustomerReferenceID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [varchar](100) NOT NULL,
	[Address] [varchar](1000) NULL,
	PRIMARY KEY ([CustomerReferenceID]))

CREATE TABLE [dbo].[BookingDetails](
	[BookingReferenceID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerReferenceID] [int] NOT NULL,
	[ArrivalDate] [datetime] NOT NULL,
	[DepartureDate] [datetime] NOT NULL
	PRIMARY KEY ([BookingReferenceID]),
	CONSTRAINT [FK_CustomerDetails] FOREIGN KEY ([CustomerReferenceID]) REFERENCES CustomerDetails([CustomerReferenceID])
	)

CREATE TABLE [dbo].[BookingGuestDetails](
	[BookingGuestID] [int] IDENTITY(1,1) NOT NULL,
	[BookingReferenceID] [int] NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[PassportNumber] [varchar](10) NOT NULL,
	[Age] [int] NOT NULL
	PRIMARY KEY ([BookingGuestID]),
	CONSTRAINT [FK_BookingDetails_Guest] FOREIGN KEY ([BookingReferenceID]) REFERENCES BookingDetails([BookingReferenceID])
	)

CREATE TABLE [dbo].[BookingExtrasDetails](
	[BookingExtrasDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[BookingReferenceID] [int] NOT NULL,
	[HasOptedSameForAllDays] BIT NOT NULL DEFAULT 0,
	[Date] [datetime] NULL,
	[HasOptedForBreakfast] [bit] NOT NULL DEFAULT 0,
	[BreakfastRequirements] [varchar](100) NULL,
	[HasOptedForEveningMeal] [bit] NOT NULL DEFAULT 0,
	[EveningMealRequirements] [varchar](100) NULL
	PRIMARY KEY ([BookingExtrasDetailsID]),
	CONSTRAINT [FK_BookingDetails_Extras] FOREIGN KEY ([BookingReferenceID]) REFERENCES BookingDetails([BookingReferenceID])
	)

CREATE TABLE [dbo].[BookingCarHireDetails](
	[CarHireDetailsDetailsID] [int] IDENTITY(1,1) NOT NULL,
	[BookingReferenceID] [int] NOT NULL,
	[Days] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[DriverName] [varchar](100) NOT NULL,
	PRIMARY KEY ([CarHireDetailsDetailsID]),
	CONSTRAINT [FK_BookingDetails_Car] FOREIGN KEY ([BookingReferenceID]) REFERENCES BookingDetails([BookingReferenceID])
	)